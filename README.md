Tries to establish similarities in motion vectors from two different videos using KMeans. Motion vectors are obtained using Dense Optical Flow.

Uses openCV-python and sklearn implementation of kMeans.