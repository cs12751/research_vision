import matplotlib.pyplot as plt
import matplotlib.cm as cmap
import numpy as np
import cv2
import sys
import os
import pylab
from random import randint
from scipy.cluster.vq import *
import colorsys
from sklearn.cluster import KMeans
import pylab 

np.set_printoptions(threshold=np.nan)

def draw_flow(img, flow, step=16):
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    #vis = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.polylines(img, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(img, (x1, y1), 1, (0, 255, 0), -1)
    return img

def draw_hsv(flow):
    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return bgr

def showFrame(frame):
	cv2.imshow('Frame',frame)
	cv2.waitKey(0)
	cv2.destroyAllWindows()	

def fcount(path):
    count = 0
    for root, dirs, files in os.walk(path):
            count += len(dirs)

    return count

def applyThreshold(src, value):
	src[np.abs(src) < value] = 0
	return src


def readInputs(locationNumber, snippetNumber, fileType) :
	#fileType: "default" or "cropped" or "cropped_order"	
	framesArray = []

	for f in os.listdir(locationNumber + "/" + snippetNumber + "/"):
		if (f[0].isdigit() and fileType == "default" ):
			framesArray.append(f)
		elif (f.startswith(fileType) and (not f.startswith("cropped_order")) and fileType == "cropped" and f.endswith("png")):
			framesArray.append(f)
		elif (f.startswith(fileType) and fileType == "cropped_order"):
			framesArray.append(f)
	
	return sorted(framesArray)


def calcDenseOpticalFlow(locationNumber, snippetNumber, frames, X):
	#first frame
	frame = cv2.imread(locationNumber + "/" + snippetNumber + "/" + frames[0])
	frameGray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

	for i in range(1, len(frames)):	
		print ("frame",i)
		#next frame
		frame1 = cv2.imread(locationNumber + "/" + snippetNumber + "/" + frames[i])
		frame1Gray = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)

		#compute flow, returns (magnitude, direction)
		flow = cv2.calcOpticalFlowFarneback(frameGray, frame1Gray, 0.5,1,3,15,3,5,1)
		
		#compute magnitude & angle
		(mag, ang) = cv2.cartToPolar(flow[...,0], flow[...,1])
		
		#apply threshold
		mag = applyThreshold(mag, 250)

		frame = frame1

		mag = np.reshape(mag, (len(mag)*len(mag[0]),1))
		ang = np.reshape(ang, (len(ang)*len(ang[0]),1))
		
		temp = np.hstack((mag, ang))
		X = np.vstack((X,temp))
	return X
#########################################################################
######################### END HELPER FUNCTIONS ##########################

location = "Location1"
fileType = "cropped"

X = np.empty(shape=[0,2])
motionVectorCount = []
#for i in  range(1, fcount( location + "/" ) + 1):
i = 1
print "Starting Snippet" + str(i)
frames = readInputs(location, "Snippet" + str(i), fileType)
X = calcDenseOpticalFlow(location, "Snippet" + str(i), frames, X)
X = X[ X[:, 0] != 0 ] #discard all 0 values

i1 = 7
X1 = np.empty(shape=[0,2])
print "Starting Snippet" + str(i1)
frames1 = readInputs(location, "Snippet" + str(i1), fileType)
X1 = calcDenseOpticalFlow(location, "Snippet" + str(i1), frames1, X)
X1 = X1[ X1[:, 0] != 0 ] #discard all 0 values

len1 = len(X)
len2 = len(X1)

X3 = np.empty(shape=[0,2])
X3 = np.vstack((X,X1))

clt = KMeans(30)
clt.fit(X3)

plot1 = clt.labels_[0 : len1]
plot2 = clt.labels_[len1 : ]

plt.hist(plot1, bins=30, color='b', label=str(i), normed=True)
plt.hist(plot2, bins=30, color='r', alpha=0.5, label=str(i1), normed=True)
plt.show()




